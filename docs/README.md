# GitLab + Cypress = ❤️

This example shows how to deploy a static site to [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/) and then test it using [Cypress](https://www.cypress.io/) E2E test runner.

See the full [.gitlab-ci.yml](https://gitlab.com/bahmutov/gitlab-pages-example/-/blob/main/.gitlab-ci.yml) file that shows how to:

- cache NPM modules and Cypress binary
- deploy the built static site (this site!) to GitLab Pages
- run Cypress E2E tests during the `confidence-check` stage
