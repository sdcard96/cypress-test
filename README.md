# gitlab-pages-example [![gitlab-pages-example](https://img.shields.io/endpoint?url=https://dashboard.cypress.io/badge/simple/5qpjcx/main&style=flat&logo=cypress)](https://dashboard.cypress.io/projects/5qpjcx/runs)
> Static site deployed to GitLab pages and tested using Cypress

[https://bahmutov.gitlab.io/gitlab-pages-example/](https://bahmutov.gitlab.io/gitlab-pages-example/)

Deploying the site to [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/) and then running Cypress tests against the deployed URL.

See the [.gitlab-ci.yml](./.gitlab-ci.yml) file for details
test
